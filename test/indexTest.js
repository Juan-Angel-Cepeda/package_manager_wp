/* eslint-disable no-undef */
const sumar = require("../index");
const assert = require('assert');

//pruebas, unitarias, funcionales, integración, estres
//¿cómo se escribe una prueba?


// eslint-disable-next-line no-undef
describe("Probar la suma de dos números",()=>{
    //afirmamos que cinco más cinco es diez
    // y afirmamos que cinco más cinco no son 55
    // eslint-disable-next-line no-undef
    it("cinco más cinco 10",()=>{
        assert.equal(10,sumar(5,5));
    });

    it("Cinco más cinco no son 55",()=>{
        assert.notEqual(55,sumar(5,5));
    })
});
